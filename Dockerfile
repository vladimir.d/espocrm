FROM cloudron/base:3.0.0@sha256:455c70428723e3a823198c57472785437eb6eab082e79b3ff04ea584faf46e92

RUN apt-get update && \
    apt-get install -y php-zmq && \
    rm -rf /var/cache/apt /var/lib/apt/lists

RUN mkdir -p /app/code

ENV ESPOCRM_OLD_VERSION 6.1.6
ENV ESPOCRM_VERSION 6.1.7

# latest code
RUN cd /tmp && \
    wget https://www.espocrm.com/downloads/EspoCRM-${ESPOCRM_VERSION}.zip && \
    unzip EspoCRM-${ESPOCRM_VERSION}.zip -d /tmp && \
    mkdir /app/code/${ESPOCRM_VERSION} && \
    mv /tmp/EspoCRM-${ESPOCRM_VERSION}/* /app/code/${ESPOCRM_VERSION} && \
    rm -rf EspoCRM-${ESPOCRM_VERSION}.zip /tmp/EspoCRM-${ESPOCRM_VERSION} && \
    ln -sf /app/code/${ESPOCRM_VERSION} /app/code/current && \
    chown -R www-data:www-data /app/code/${ESPOCRM_VERSION}

WORKDIR /app/code/current

RUN rm -rf /app/code/current/data && ln -s /app/data/data /app/code/current/data && \
    rm -rf /app/code/current/custom && ln -s /app/data/custom /app/code/current/custom && \
    rm -rf /app/code/current/client/custom && ln -s /app/data/client/custom /app/code/current/client/custom && \
    mv /app/code/current/application/Espo/Modules /app/code/current/application/Espo/Modules.orig && ln -s /app/data/modules/application /app/code/current/application/Espo/Modules && \
    mv /app/code/current/client/modules /app/code/current/client/modules.orig && ln -s /app/data/modules/client /app/code/current/client/modules

# previous code
RUN cd /tmp && \
    wget https://www.espocrm.com/downloads/EspoCRM-${ESPOCRM_OLD_VERSION}.zip && \
    unzip EspoCRM-${ESPOCRM_OLD_VERSION}.zip -d /tmp && \
    mkdir /app/code/${ESPOCRM_OLD_VERSION} && \
    mv /tmp/EspoCRM-${ESPOCRM_OLD_VERSION}/* /app/code/${ESPOCRM_OLD_VERSION} && \
    rm -rf EspoCRM-${ESPOCRM_OLD_VERSION}.zip /tmp/EspoCRM-${ESPOCRM_OLD_VERSION} && \
    ln -sf /app/code/${ESPOCRM_OLD_VERSION} /app/code/old && \
    chown -R www-data:www-data /app/code/${ESPOCRM_OLD_VERSION}

RUN rm -rf /app/code/old/data && ln -s /app/data/data /app/code/old/data && \
    rm -rf /app/code/old/custom && ln -s /app/data/custom /app/code/old/custom && \
    rm -rf /app/code/old/client/custom && ln -s /app/data/client/custom /app/code/old/client/custom

# Upgrade package
RUN wget https://www.espocrm.com/downloads/upgrades/EspoCRM-upgrade-${ESPOCRM_OLD_VERSION}-to-${ESPOCRM_VERSION}.zip -O /app/code/upgrade.zip

# The VOIP integration packages installs some files in the root (https://forum.cloudron.io/topic/1874/espocrm-extension-installation-failure)
RUN ln -s /app/data/voip-integration/starface.php /app/code/current/starface.php && \
    ln -s /app/data/voip-integration/voip-service.php /app/code/current/voip-service.php && \
    ln -s /app/data/voip-integration/voip.php /app/code/current/voip.php && \
    chown www-data:www-data --no-dereference /app/code/current/starface.php /app/code/current/voip-service.php /app/code/current/voip.php

# configure apache
# keep the prefork linking below a2enmod since it removes dangling mods-enabled (!)
RUN a2disconf other-vhosts-access-log && \
    echo "Listen 8000" > /etc/apache2/ports.conf && \
    a2enmod rewrite headers rewrite expires cache && \
    rm /etc/apache2/sites-enabled/* && \
    sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf && \
    ln -sf /app/data/apache/mpm_prefork.conf /etc/apache2/mods-enabled/mpm_prefork.conf

COPY apache/espocrm.conf /etc/apache2/sites-enabled/espocrm.conf

# install RPAF module to override HTTPS, SERVER_PORT, HTTP_HOST based on reverse proxy headers
# https://www.digitalocean.com/community/tutorials/how-to-configure-nginx-as-a-web-server-and-reverse-proxy-for-apache-on-one-ubuntu-16-04-server
# https://github.com/espocrm/espocrm/issues/1928
RUN mkdir /app/code/rpaf && \
    curl -L https://github.com/gnif/mod_rpaf/tarball/669c3d2ba72228134ae5832c8cf908d11ecdd770 | tar -C /app/code/rpaf -xz --strip-components 1 -f -  && \
    cd /app/code/rpaf && \
    make && \
    make install && \
    rm -rf /app/code/rpaf

# configure rpaf
RUN echo "LoadModule rpaf_module /usr/lib/apache2/modules/mod_rpaf.so" > /etc/apache2/mods-available/rpaf.load && a2enmod rpaf

# configure mod_php
# https://www.espocrm.com/blog/server-configuration-for-espocrm/
RUN a2enmod rewrite proxy proxy_wstunnel
RUN crudini --set /etc/php/7.4/apache2/php.ini PHP max_execution_time 180 && \
    crudini --set /etc/php/7.4/apache2/php.ini PHP max_input_time 180 && \
    crudini --set /etc/php/7.4/apache2/php.ini PHP memory_limit 256M && \
    crudini --set /etc/php/7.4/apache2/php.ini PHP post_max_size 50M && \
    crudini --set /etc/php/7.4/apache2/php.ini PHP upload_max_filesize 50M && \
    crudini --set /etc/php/7.4/apache2/php.ini Session session.save_path /run/espocrm/sessions

RUN cp /etc/php/7.4/apache2/php.ini /etc/php/7.4/cli/php.ini

RUN ln -s /app/data/php.ini /etc/php/7.4/apache2/conf.d/99-cloudron.ini && \
    ln -s /app/data/php.ini /etc/php/7.4/cli/conf.d/99-cloudron.ini

# supervisor
ADD supervisor/ /etc/supervisor/conf.d/
RUN sed -e 's,^logfile=.*$,logfile=/run/espocrm/supervisord.log,' -i /etc/supervisor/supervisord.conf

ADD apache/mpm_prefork.conf start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
