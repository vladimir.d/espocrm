[0.0.1]
* Initial version
* Use EspoCRM to 4.8.4

[0.1.0]
* Update EspoCRM to 5.0.5

[0.2.0]
* Update EspoCRM to 5.1.0

[0.3.0]
* Fix apache config

[0.4.0]
* Run cronjob on startup to fix warning display

[0.4.1]
* Update logo

[0.5.0]
* Disable new version notification

[0.5.1]
* Protect data directory in apache configs

[0.5.2]
* Fix issue where was not starting after a restart

[1.0.0]
* Update EspoCRM to 5.2.5
* [Announcement](https://www.espocrm.com/blog/espocrm-5-2-0-released/)
* Data Privacy: Ability to view personal data;
* Data Privacy: Ability to erase personal data;
* Data Privacy: Parameter to mark new email addresses opted out by default;
* Shared Calendar;
* Kanban view;
* Icons in the vertical navbar;
* Specific colors for entity types (for a quicker recognition);
* Entity Manager: Ability to pick an icon for an entity type;
* Entity Manager: Ability to reset to defaults;
* Calendar: Displaying users avatars on the timeline view;
* Opportunities: Convert Currency list view action.

[1.1.0]
* Update EspoCRM to 5.3.2
* [Announcement](https://www.espocrm.com/blog/espocrm-5-3-0-released/)
* Full-text search;
* Mail Merge;
* Layout Manager: Dynamic logic for panels visibility;
* Layout Manager: Ability to remove default side panel or move it below other panels;
* Supporting link-multiple fields on list views;
* Mass Print to PDF action from list view;
* Print to PDF: Ability to print link-multiple fields;
* Text Filter: Supporting Auto-increment and Integer fields;
* Opportunity: Last Stage field; Ability to show sales pipeline based on the last stage field;
* Formula: countRelated, sumRelated functions.
* Date/DateTime fields: ‘Use Numeric Format’ parameter.

[1.1.1]
* Update EspoCRM to 5.3.4

[1.1.2]
* Update EspoCRM to 5.3.5

[1.1.3]
* Update EspoCRM to 5.3.6

[1.2.0]
* Update EspoCRM to 5.4.1

[1.2.1]
* Update EspoCRM to 5.4.2

[1.2.2]
* Update EspoCRM to 5.4.3

[1.3.0]
* Use latest base image

[1.3.1]
* Update EspoCRM to 5.4.4

[1.3.2]
* Update EspoCRM to 5.4.5

[1.4.0]
* Update EspoCRM to 5.5.1
* Ability to search in related record list;
* Activities: Ability to search in Activities and History of a specific record;
* Stream: Ability to search posts in record's stream;
* Template manager (customizing email messages sent by application);
* Enum field: Ability to specify color style for options;
* Ability to define autocomplete country list;
* Varchar field: Ability to define autocomplete suggestions;
* Email Templates: Today and Now placeholders;
* Timeline: Displaying busy timeframes in shared view;

[1.4.1]
* Update EspoCRM to 5.5.3

[1.4.2]
* Update EspoCRM to 5.5.4

[1.4.3]
* Update EspoCRM to 5.5.5

[1.4.4]
* Update EspoCRM to 5.5.6

[1.5.0]
* Update EspoCRM to 5.6.0
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/5.6.0)
* WebSocket support;
* Ability to restore deleted records for admin;
* 'Recalculate Formula' mass action for admin;
* Ability to mass unlink related records;
* Ability to view & manage a list of followers of a specific record;
* Meetings: All-day meetings;
* Meetings/Calls: Acceptance button;
* Meetings/Calls: 'Set Held' and 'Set Not Held' actions on quick view modal;
* Phone Numbers: Invalid and Opted-Out attributes;
* Emails: Ability to view/manage user list, email record is available for;

[1.5.1]
* Update EspoCRM to 5.6.1
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/5.6.1)
* WebSocket: Not working event popup notifications

[1.5.2]
* Update EspoCRM to 5.6.2
* Import: Multiple email addresses support;
* Foreign field: Email and Phone field types support;
* Lead Capture: Ability to view payload data in log;
* Lead Capture: Ability to disable duplicate check.

[1.6.0]
* Update manifest to v2

[1.6.1]
* Update EspoCRM to 5.6.3

[1.6.2]
* Update EspoCRM to 5.6.4

[1.6.3]
* Update EspoCRM to 5.6.5
* Fix XSS issue

[1.6.4]
* Update EspoCRM to 5.6.6

[1.6.5]
* Update EspoCRM to 5.6.7

[1.6.6]
* Update EspoCRM to 5.6.8

[1.6.7]
* Update EspoCRM to 5.6.9

[1.6.8]
* Make module installation work

[1.7.0]
* Update EspoCRM to 5.6.11

[1.7.1]
* Update EspoCRM to 5.6.12

[1.7.2]
* Update EspoCRM to 5.6.13

[1.7.3]
* Update EspoCRM to 5.6.14

[1.8.0]
* Update EspoCRM to 5.7.0

[1.8.1]
* Update EspoCRM to 5.7.1

[1.8.2]
* Update EspoCRM to 5.7.2

[1.8.3]
* Update EspoCRM to 5.7.3

[1.8.4]
* Update EspoCRM to 5.7.4

[1.8.5]
* Update EspoCRM to 5.7.5

[1.8.6]
* Update EspoCRM to 5.7.6

[1.8.7]
* Update EspoCRM to 5.7.7

[1.8.8]
* Update EspoCRM to 5.7.8

[1.8.9]
* Update EspoCRM to 5.7.9

[1.8.10]
* Update EspoCRM to 5.7.10

[1.8.11]
* Update EspoCRM to 5.7.11

[1.8.12]
* Preserve aclStrictMode

[1.9.0]
* Update EspoCRM to 5.8.0

[1.9.1]
* Update EspoCRM to 5.8.1

[1.9.2]
* Update EspoCRM to 5.8.2

[1.9.3]
* Update EspoCRM to 5.8.3

[1.9.4]
* Update EspoCRM to 5.8.4

[1.10.0]
* Add support for voip integration

[1.11.0]
* Enable web socket support

[1.11.1]
* Update EspoCRM to 5.8.5

[1.12.0]
* Use latest base image 2.0.0

[1.13.0]
* Update EspoCRM to 5.9.0
* [Full changes](https://github.com/espocrm/espocrm/releases/tag/5.9.0)

[1.13.1]
* Update EspoCRM to 5.9.1
* [Full changes](https://github.com/espocrm/espocrm/releases/tag/5.9.1)
* Mass Email: Not sending if VERP is enabled bug

[1.13.2]
* Update EspoCRM to 5.9.2
* [Full changes](https://github.com/espocrm/espocrm/releases/tag/5.9.2)
* Roles: Edit view improvements #1707
* Scheduled jobs: Showing human readable description of the scheduling #1712
* Scheduled jobs: Validation for scheduling expression #1713
* Relationship panel: 'Create' button on 'Select' dialog #1724
* Password recovery: Parameter to prevent email address exposure on recovery form #1728
* Password recovery: Parameter to disable password recovery for all internal users #1729

[1.13.3]
* Update EspoCRM to 5.9.3
* [Full changes](https://github.com/espocrm/espocrm/releases/tag/5.9.3)
* Vertical navbar not scrollable in some cases bug
* Select followers button is not working bug

[1.13.4]
* Improve apache handling

[1.13.5]
* Update EspoCRM to 5.9.4
* [Full changes](https://github.com/espocrm/espocrm/releases/tag/5.9.4)
* Formula: Numbers with zero decimal part are parsed as integers bug
* Markdown: Links inside code blocks displayed incorrectly bug
* Date field may display values with 1-day shift in some cases bug
* Formula: Result of dayOfWeek function depends on language set in settings bug
* Formula: record\count & record\exists do not support usage of a same where key multiple times bug
* Formula: Using quote characters within comments breaks the script bug

[1.14.0]
* Update EspoCRM to 6.0.0
* [Full changes](https://github.com/espocrm/espocrm/releases/tag/6.0.0)
* Import: Ability to run import with parameters of a previously run import #1736
* Import: Ability to resume failed import from the last processed row #1735
* Import: Ability to run from CLI #1734
* Emails: Ability to insert record field values when compose #1730
* Admin panel: Quick search #1756
* Console: Command printing all available container services #1748
* Formula: Language translate functions #1783
* Formula: json\retrieve function #1796
* Image field: 'Preview size in list view' parameter #1799
* Campaign Log: Search by email address and queue item ID #1811
* Emails: Hiding modal window dialog after Send button is clicked #1737
* Mass Email: Teams of campaign record are copied to all sent emails #1780
* Email Accounts: TLS support for IMAP #1791
* Admin panel: Quick search #1756

[1.14.1]
* Update EspoCRM to 6.0.1
* Label Manager: Save does not work #1813

[1.14.2]
* Update EspoCRM to 6.0.2
* [Full changes](https://github.com/espocrm/espocrm/releases/tag/6.0.2)

[1.14.3]
* Update EspoCRM to 6.0.3
* [Full changes](https://github.com/espocrm/espocrm/milestone/102?closed=1)

[1.14.4]
* Update EspoCRM to 6.0.4
* Added currency format '10 €' #1830

[1.14.5]
* Update EspoCRM to 6.0.5
* Fixed: Duplicating record with attachment-multiple fields causing attachment being detached from their field
* Fixed: Searching by person name is not working when format is Last First Middle
* Fixed: Export is not available for admin if disabled in settings

[1.14.6]
* Update EspoCRM to 6.0.6
* Fixed: XLSX Export error when there are rows with empty currency

[1.14.7]
* Update EspoCRM to 6.0.7
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/6.0.7)

[1.14.8]
* Update EspoCRM to 6.0.8
* Category tree paths being created improperly
* Global auto-follow can't be saved bug
* Notifications about posts don't work for portal users bug

[1.14.9]
* Update EspoCRM to 6.0.9
* [Full changelog](https://github.com/espocrm/espocrm/milestone/108?closed=1)

[1.14.10]
* Update EspoCRM to 6.0.10
* [Full changelog](https://github.com/espocrm/espocrm/milestone/110?closed=1)

[1.15.0]
* Update EspoCRM to 6.1.0
* [Release Announcement](https://forum.espocrm.com/forum/announcements/66733-espocrm-6-1-0-released)
* Navbar: Tab groups #1848
* Log: Ability to specify which handlers to use #1861
* Kanban: Ability to sort items within a group #1825
* Array field: Ability to add multiple items at once #1874
* Entity Manager: Separate page for entity #1833
* List view: Showing 'Apply' button when filter added, removed or changed #1831

[1.16.0]
* [Support custom domains for portals](https://docs.cloudron.io/apps/espocrm/#portals)

[1.16.1]
* Update EspoCRM to 6.1.1
* [Full changelog](https://github.com/espocrm/espocrm/milestone/111?closed=1)

[1.16.2]
* Update EspoCRM to 6.1.2
* Fix Kanban ordering

[1.17.0]
* Use base image v3
* Update PHP to 7.4
* cron is now run every minute

[1.17.1]
* Add a log for cron jobs

[1.17.2]
* Update EspoCRM to 6.1.3
* Save & Continue Editing action for PDF templates, email templates and knowledge base #1919
* Formula: array\join function #1917
* Formula: util\generateId function #1912

[1.17.3]
* Update EspoCRM to 6.1.4
* Printing to PDF not working
* Attachment size not properly calculated

[1.17.4]
* Update EspoCRM to 6.1.5

[1.17.5]
* Update EspoCRM to 6.1.6

[1.17.6]
* Set `REMOTE_ADDR` to the real IP address

[1.17.7]
* Update EspoCRM to 6.1.7

