#!/usr/bin/env node

/* jshint esversion: 8 */
/* jslint node:true */
/* global it:false */
/* global xit:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

require('chromedriver');

var execSync = require('child_process').execSync,
    expect = require('expect.js'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = 'test';
    const TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 10000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const USERNAME = process.env.USERNAME;
    const PASSWORD = process.env.PASSWORD;

    var app;
    var browser;
    var contactId;

    before(function () {
        browser = new Builder().forBrowser('chrome').setChromeOptions(new Options().windowSize({ width: 1280, height: 1024 })).build();
    });

    after(function () {
        browser.quit();
    });

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TIMEOUT);
    }

    function sleep(millis) {
        return new Promise(resolve => setTimeout(resolve, millis));
    }

    async function login(username, password) {
        await browser.get('https://' + app.fqdn);

        await browser.wait(until.elementLocated(By.id('field-userName')), TIMEOUT);
        await browser.findElement(By.id('field-userName')).sendKeys(username);
        await browser.findElement(By.id('field-password')).sendKeys(password);
        await browser.findElement(By.id('btn-login')).click();
        await browser.wait(until.elementLocated(By.xpath('//span[text()="Opportunities"]')), TIMEOUT);
    }

    async function adminLogin() {
        await login('admin', 'changeme');
    }

    async function createContact() {
        await browser.get('https://' + app.fqdn + '/#Account/create');

        await waitForElement(By.xpath('//input[@data-name="name"]'));
        await browser.findElement(By.xpath('//input[@data-name="name"]')).sendKeys('cloudroncontact');
        await browser.findElement(By.xpath('//button[text()="Save"]')).click();
        await browser.wait(function () {
            return browser.getCurrentUrl().then(function (url) {
                contactId = url.slice(url.lastIndexOf('/')+1);
                return url.startsWith('https://' + app.fqdn + '/#Account/view/');
            });
        }, TIMEOUT);

        console.log(`Contact ID is ${contactId}`);
    }

    async function contactExists() {
        await browser.get(`https://${app.fqdn}/#Account/view/${contactId}`);
        await waitForElement(By.xpath('//div[text()="cloudroncontact"]'));
    }

    async function logout() {
        await browser.get('https://' + app.fqdn);

        await waitForElement(By.xpath('//a[@id="nav-menu-dropdown"]'));
        await browser.findElement(By.xpath('//a[@id="nav-menu-dropdown"]')).click();

        await waitForElement(By.xpath('//a[text()="Log Out"]'));
        await browser.findElement(By.xpath('//a[text()="Log Out"]')).click();

        await sleep(5000);
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });

    it('install app', async function () {
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        await sleep(10000); // wait for sometime for the setup to finish
    });

    it('can get app information', getAppInfo);

    // this allows a normal user to access all views. otherwise, we have to create roles etc
    it('can disable acl', function () {
        execSync(`cloudron exec --app ${app.fqdn} -- sed -e 's/.*aclStrictMode.*/    "aclStrictMode" => false,/g' -i /app/data/data/config.php`, EXEC_ARGS);
    });

    it('can admin login', adminLogin);
    it('can logout', logout);

    it('can login', login.bind(null, USERNAME, PASSWORD));
    it('can create contact', createContact);
    it('can check contact', contactExists);
    it('can logout', logout);

    it('can restart app', async function () {
        execSync('cloudron restart --app ' + app.id);
        await sleep(10000);
    });

    it('can admin login', adminLogin);
    it('can logout', logout);

    it('can login', login.bind(null, USERNAME, PASSWORD));
    it('can logout', logout);

    it('can backup app', function () { execSync('cloudron backup create --app ' + app.id, EXEC_ARGS); });
    it('restore app', async function () {
        await browser.get('about:blank');
        const backups = JSON.parse(execSync('cloudron backup list --raw --app ' + app.id));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
        await sleep(10000);
    });

    it('can admin login', adminLogin);
    it('can logout', logout);

    it('can login', login.bind(null, USERNAME, PASSWORD));
    it('can check contact', contactExists);
    it('can logout', logout);

    it('move to different location', async function () {
        await browser.get('about:blank');
        execSync('cloudron configure --location ' + LOCATION + '2 --app ' + app.id, EXEC_ARGS);
        getAppInfo();
    });

    it('can admin login', adminLogin);
    it('can logout', logout);

    it('can login', login.bind(null, USERNAME, PASSWORD));
    it('can check contact', contactExists);
    it('can logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });

    // test update
    it('can install app for update', async function () {
        execSync('cloudron install --appstore-id com.espocrm.cloudronapp --location ' + LOCATION, EXEC_ARGS);
        await sleep(10000); // wait for sometime for the setup to finish
    });
    it('can get app information', getAppInfo);
    it('can admin login', adminLogin);
    it('can logout', logout);

    it('can disable acl', function () {
        execSync(`cloudron exec --app ${app.fqdn} -- sed -e 's/.*aclStrictMode.*/    "aclStrictMode" => false,/g' -i /app/data/data/config.php`, EXEC_ARGS);
    });
    it('can restart app for setting to take effect', async function () {
        execSync('cloudron restart --app ' + app.id);
        await sleep(10000);
    });
    it('can login', login.bind(null, USERNAME, PASSWORD));
    it('can create contact', createContact);
    it('can logout', logout);

    it('can update', async function () {
        execSync('cloudron update --app ' + LOCATION, EXEC_ARGS);
        await sleep(10000);
    });

    it('can admin login', adminLogin);
    it('can logout', logout);

    it('can login', login.bind(null, USERNAME, PASSWORD));
    it('can check contact', contactExists);
    it('can logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });
});

