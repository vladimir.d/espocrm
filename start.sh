#!/bin/bash

set -eu

# setup waits for apache to start and configures ttrss
setup() {
    while [[ ! -f "/var/run/apache2/apache2.pid" ]]; do
        echo "=> Waiting for apache2 to start"
        sleep 1
    done

    echo "=> First run, saving DB settings"
    curl --data "host-name=mysql&db-name=${CLOUDRON_MYSQL_DATABASE}&db-user-name=${CLOUDRON_MYSQL_USERNAME}&db-user-password=${CLOUDRON_MYSQL_PASSWORD}&action=saveSettings" http://localhost:8000/install/

    echo "=> Initializing database"
    curl --data "action=buildDatabase" http://localhost:8000/install/

    echo "=> Creating admin user"
    curl --data "name=admin&pass=changeme&configPass=changeme&user-name=admin&user-pass=changeme&action=createUser" http://localhost:8000/install/

    echo "${ESPOCRM_VERSION}" > /app/data/data_version
}

# maybe this can be changed to patch config.php code to have getenv() directly
update_config() {
    echo "=> update config"
    sudo -u www-data --preserve-env php <<'EOF'
<?php
    include "bootstrap.php";

    $config = require("/app/data/data/config.php");
    $db = parse_url(getenv('CLOUDRON_MYSQL_URL'));
    $runtime_config = [
        'database' => [
            'driver' => 'pdo_mysql',
            'host' =>  $db['host'],
            'port' => '',
            'charset' => 'utf8mb4',
            'dbname' => substr($db['path'], 1),
            'user' => $db['user'],
            'password' =>  $db['pass']
        ],
        'adminNotificationsNewVersion' => false,
        'useWebSocket' => true,
        'smtpServer' => getenv('CLOUDRON_MAIL_SMTP_SERVER'),
        'smtpPort' => getenv('CLOUDRON_MAIL_SMTP_PORT'),
        'smtpAuth' => true,
        'smtpSecurity' => '',
        'smtpUsername' => getenv('CLOUDRON_MAIL_SMTP_USERNAME'),
        'smtpPassword' => getenv('CLOUDRON_MAIL_SMTP_PASSWORD'),
        'outboundEmailFromAddress' => getenv('CLOUDRON_MAIL_FROM'),
        'siteUrl' => getenv('CLOUDRON_APP_ORIGIN'),
        'isInstalled' => true,
        # https://framework.zend.com/manual/1.12/en/zend.ldap.api.html
        'authenticationMethod' => 'LDAP',
        'ldapHost' => getenv('CLOUDRON_LDAP_SERVER'),
        'ldapPort' => getenv('CLOUDRON_LDAP_PORT'),
        'ldapUsername' => getenv('CLOUDRON_LDAP_BIND_DN'),
        'ldapPassword' => getenv('CLOUDRON_LDAP_BIND_PASSWORD'),
        'ldapBindRequiresDn' => true,
        'ldapBaseDn' => getenv('CLOUDRON_LDAP_USERS_BASE_DN'),
        'ldapUserNameAttribute' => 'username',
        'ldapUserObjectClass' => 'user',
        'ldapUserLoginFilter' => '',
        'ldapCreateEspoUser' => true,
        'ldapAuth' => true,
        'ldapSecurity' => '',
        'ldapAccountCanonicalForm' => 'Dn',
        'ldapTryUsernameSplit' => false,
        'ldapOptReferrals' => false,
        'ldapUserTeamsIds' => [
        ],
        'ldapUserTeamsNames' => (object) [
        ],
        'ldapUserDefaultTeamName' => NULL,
        'ldapUserDefaultTeamId' => NULL,
    ];
    $config = array_replace($config, $runtime_config);
    $fm = new \Espo\Core\Utils\File\Manager();
    file_put_contents("/app/data/data/config.php", $fm->wrapForDataExport($config));
EOF
}

upgrade() {
    # apply upgrade in a temporary directory
    data_version=""
    if [[ -f /app/data/data_version ]]; then
        data_version="$(cat /app/data/data_version)"
    fi
    echo "=> Data version is ${data_version}. Expecting ${ESPOCRM_VERSION}"

    if [[ "${data_version}" == "${ESPOCRM_VERSION}" ]]; then
        echo "=> Up to date. Nothing to upgrade"
        return 0
    fi

    echo "=> Running upgrade script"
    rm -rf /run/espocrm/old && mkdir /run/espocrm/old && cp -r  /app/code/old/* /run/espocrm/old && chown -R www-data:www-data /run/espocrm/old
    upgrade_output=$(cd /run/espocrm/old && /usr/local/bin/gosu www-data:www-data php /run/espocrm/old/upgrade.php /app/code/upgrade.zip)
    if [[ "${upgrade_output}" != *"Upgrade is complete."* ]]; then
        echo -e "=> Failed to upgrade.\n----- Upgrade output -----\n ${upgrade_output}\n-----\n"
        return 1
    else
        echo -e "=> Upgraded successfully!\n----- Upgrade output -----\n${upgrade_output}\n-----\n"
        rm -rf /run/espocrm/old
    fi

    echo "${ESPOCRM_VERSION}" > /app/data/data_version
}

run_cron() {
    # This prevents error message about cron being run on the admin page on installation
    echo "=> Running cron job"
    /usr/local/bin/gosu www-data:www-data php /app/code/current/cron.php
}

mkdir -p /run/espocrm/sessions /run/espocrm/logs /run/espocrm/cache /app/data/data /app/data/client/custom /app/data/custom /app/data/modules/application /app/data/modules/client /app/data/voip-integration /app/data/apache

rm -rf /app/data/data/logs && ln -sf /run/espocrm/logs /app/data/data/logs
rm -rf /app/data/data/cache && ln -sf /run/espocrm/cache /app/data/data/cache
echo "=> Fixing up modules"
for f in $(ls /app/code/current/application/Espo/Modules.orig); do
    rm -f "/app/data/modules/application/$f"
    ln -sf /app/code/current/application/Espo/Modules.orig/$f "/app/data/modules/application/$f"
done
for f in $(ls /app/code/current/client/modules.orig); do
    rm -f "/app/data/modules/client/$f"
    ln -sf /app/code/current/client/modules.orig/$f "/app/data/modules/client/$f"
done

if [[ ! -f /app/data/php.ini ]]; then
    echo -e "; Add custom PHP configuration in this file\n; Settings here are merged with the package's built-in php.ini\n\n" > /app/data/php.ini
fi

[[ ! -f /app/data/apache/mpm_prefork.conf ]] && cp /app/pkg/mpm_prefork.conf /app/data/apache/mpm_prefork.conf

# empty files required for the VoIP plugin to install
touch /app/code/current/starface.php /app/code/current/voip-service.php /app/code/current/voip.php

# optional portals configuration
if [[ ! -f /app/data/apache/portals.conf ]]; then
    echo -e "# Add mod rewrite rules for portals here - https://docs.espocrm.com/administration/portal/\n" > /app/data/apache/portals.conf
fi

chown -R www-data:www-data /app/data /run/espocrm

if [[ ! -f /app/data/data_version ]]; then
    echo "=> Starting apache for setup"
    APACHE_CONFDIR="" source /etc/apache2/envvars
    rm -f "${APACHE_PID_FILE}"
    ( /usr/sbin/apache2 -DFOREGROUND ) &
    apache_pid=$!
    setup
    echo "=> Setup complete"
    kill ${apache_pid}
fi

if ! update_config; then
    echo "=> Could not update configuration"
    exit 1
fi

if ! upgrade; then
    echo "=> Could not upgrade"
    exit 1
fi

echo "=> Rebuild backend and clear cache"
/usr/local/bin/gosu www-data:www-data php /app/code/current/rebuild.php

run_cron

echo "=> Starting EspoCRM"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i EspoCRM

